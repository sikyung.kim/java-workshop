# Hello World

This is the first slide

---vertical---

# Nice vertical slide!

---slide---

# Code example
## with
### Highlighting
```html [1|2-3]
<div class="reveal">
    <div class="slides">
        <section data-markdown="slides.md"></section>
    </div>
</div>
```
Some text `with` highlighting

---slide---


<!-- .slide: data-background="#ff0000" -->
## Continue to see fade-in items

- Item 1<!-- .element: class="fragment" data-fragment-index="1" -->
- Item 2<!-- .element: class="fragment" data-fragment-index="2" -->
- Item 3 <!-- .element: class="fragment" data-fragment-index="3" -->
- Item 4 <!-- .element: class="fragment" data-fragment-index="4" -->

---slide---

# And the final slide!

